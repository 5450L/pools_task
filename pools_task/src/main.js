import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import clickOutSide from './directives/clickOutside'

const app = createApp(App);

app.use(store);
app.use(router);
app.directive('click-outside', clickOutSide)
app.mount('#app');
