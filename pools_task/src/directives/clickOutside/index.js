const clickOutSide = {
    mounted(el, binding) {
        el.clickOutSideEvent = (event) => {
            if (!(el == event.target || el.contains(event.target))) {
                binding.value(event, el);
            }
        };
        document.addEventListener('click', el.clickOutSideEvent)
    },
    unmounted(el) {
        document.removeEventListener('click', el.clickOutSideEvent)
    },
}

export default clickOutSide;