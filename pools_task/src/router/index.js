import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/limone',
    name: 'limone',
    component: (() => import('@/components/Pools/LimonePools.vue'))
  },
  {
    path: '/w3optimizer',
    name: 'w3optimizer',
    component: (() => import('@/components/Pools/W3Optimizer/W3OptimizerPools.vue'))
  },
  {
    path: "/w3optimizer/:id",
    name: "poolW3",
    component: () => import("@/components/Pools/W3Optimizer/PoolW3.vue"),
  },
  {
    path: '/farm',
    name: 'farm',
    component: (() => import('@/components/Pools/FarmPools.vue'))
  },
  {
    path: '/mypositions',
    name: 'mypositions',
    component: (() => import('@/components/Pools/MyPositions.vue'))
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
