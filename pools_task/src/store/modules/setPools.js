export default {
  state: {
    pools: [],
    poolsLoading: true,
  },
  mutations: {
    setPools(state, payload) {
      state.pools = payload;
    },
    setPoolsLoading(state, payload) {
      state.poolsLoading = payload;
    },
  },

  actions: {
    setPoolsAction({ commit }, pools) {
      commit('setPools', pools)

    },
    setPoolsLoadingAction({ commit }, payload) {
      commit('setPoolsLoading', payload)
    }
  },

  getters: {
    getPools: (state) => state.pools,
    getPoolsLoading: (state) => state.poolsLoading,
    getPoolById: state => id => { return state.pools.find(pool => pool.id == id) }
  },
};
