import { createStore } from 'vuex'

import setPools from'./modules/setPools'

export default createStore({
  state: {},
  getters: {},
  mutations: {},
  actions:{},
  modules: {
    setPools
  }
})
