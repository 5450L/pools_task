import poolsConfig from '@/utils/config.js'
import abiERC20 from "@/utils/abi/abiERC20";

import store from '@/store'

import { markRaw } from 'vue'
import { ethers } from 'ethers'

const connectMetamask = async () => {
    const provider = markRaw(new ethers.providers.Web3Provider(
        window.ethereum,
        "any"
    ))

    await provider.send("eth_requestAccounts", []);

    const signer = provider.getSigner();

    console.log('signer', signer)

    return signer;
}

const createPools = async () => {
    const signer = await connectMetamask();

    const pools = markRaw(
        await Promise.all(
            poolsConfig.map((poolInfo) => createPool(poolInfo, signer))
        )
    )
    store.dispatch('setPoolsAction', pools)
    store.dispatch('setPoolsLoadingAction', false)
    console.log(store)
    // return pools
}

const createPool = async (poolInfo, signer) => {

    const poolContract = new ethers.Contract(
        poolInfo.contract.address,
        JSON.stringify(poolInfo.contract.abi),
        signer
    );

    const token0Adress = await poolContract.token0();
    const token1Adress = await poolContract.token1();

    const token0Contract = new ethers.Contract(
        token0Adress,
        JSON.stringify(abiERC20),
        signer
    );
    const token1Contract = new ethers.Contract(
        token1Adress,
        JSON.stringify(abiERC20),
        signer
    );

    const token0Name = await token0Contract.name()
    const token0Decimals = await token0Contract.decimals()
    const token0TotalSupply = await token0Contract.totalSupply()

    const token1Name = await token1Contract.name()
    const token1Decimals = await token1Contract.decimals()
    const token1TotalSupply = await token1Contract.totalSupply()

    return {
        id: poolInfo.id,
        isNew: poolInfo.isNew,
        isDeprecated: poolInfo.isDeprecated,
        images: poolInfo.images,
        token0: {
            name: token0Name,
            decimals: token0Decimals,
            totalSupply: token0TotalSupply,
        },
        token1: {
            name: token1Name,
            decimals: token1Decimals,
            totalSupply: token1TotalSupply,
        },
    }
}

export { createPools }