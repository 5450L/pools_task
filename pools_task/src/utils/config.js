// import abiERC20 from "@/utils/abi/abiERC20";
import SorbettoFragolaAbi from "@/utils/abi/SorbettoFragola";

export default [
    {
        id: 1,
        isNew: true,
        isDeprecated: false,
        images: require('@/assets/images/tokens/Token_DYDX_WETH.png'),
        contract: {
            address: "0xaE7b92C8B14E7bdB523408aE0A6fFbf3f589adD9",
            abi: SorbettoFragolaAbi,
        }
    },
    {
        id: 2,
        isNew: true,
        isDeprecated: false,
        images: require('@/assets/images/tokens/Token_ELON_WETH.png'),
        contract: {
            address: "0x9683D433621A83aA7dd290106e1da85251317F55",
            abi: SorbettoFragolaAbi,
        }
    },
    {
        id: 3,
        isNew: true,
        isDeprecated: false,
        images: require('@/assets/images/tokens/Token_FTM_WETH.png'),
        contract: {
            address: "0xa1BE64Bb138f2B6BCC2fBeCb14c3901b63943d0E",
            abi: SorbettoFragolaAbi,
        }
    },
    {
        id: 4,
        isNew: true,
        isDeprecated: false,
        images: require('@/assets/images/tokens/Token_ICE_BSC.png'),
        contract: {
            address: "0x8d8B490fCe6Ca1A31752E7cFAFa954Bf30eB7EE2",
            abi: SorbettoFragolaAbi,
        }
    },
    {
        id: 5,
        isNew: false,
        isDeprecated: true,
        images: require('@/assets/images/tokens/Token_SHIB_WETH.png'),
        contract: {
            address: "0x212Aa024E25A9C9bAF5b5397B558B7ccea81740B",
            abi: SorbettoFragolaAbi,
        }
    },
    {
        id: 6,
        isNew: false,
        isDeprecated: true,
        images: require('@/assets/images/tokens/Token_ELON_WETH.png'),
        contract: {
            address: "0xBE5d1d15617879B22C7b6a8e1e16aDD6d0bE3c61",
            abi: SorbettoFragolaAbi,
        }
    }]